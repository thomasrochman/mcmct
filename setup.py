from setuptools import setup

setup(name='mcmct',
	version='0.1',
	description='Algorithm to track multiple objects in a video, by Markov Chain Monte Carlo.',
	url='https://bitbucket.org/tlb-intern2017/mcmctracker',
	author='Thomas Rochman',
	author_email='ttr5n@virginia.edu',
	license='teamLab',
	packages=['mcmct'],
	zip_safe=False)