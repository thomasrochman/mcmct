# MCMC Tracking #

## Purpose ##

In order to track multiple objects, this program uses the tracking algorithm as described in the MCMC tracking algorithm paper, Markov Chain Monte Carlo Data Association for Multiple-Target Tracking by Songhwai Oh, Stuart Russell, and Shankar Sastry.


## Setup and Environment ##

### Requirements ###

* __Python 3.6__, __Python 3.5__ or __Python 2.7.13__
* tqdm
* numpy
* scipy
* pandas

### Setup ###
In order to setup the program to work on a Python environment, you must install the packages above through the console. Insert the following:
```
#!bash

pip install tqdm
pip install numpy
pip install scipy
pip install pandas
```

First pull the directory from this repository:

```
#!bash

git pull https://bitbucket.org/tlb-intern2017/mcmctracker
```

Then cd into the repository and pip install the library into your system:
```
#!bash

cd mcmct
pip install -e .
```

## How to Use ##

Import the module into your program through the following commands:

```
#!python

from mcmct import MCMCalg as mca
from mcmct import shiftMCMCalg as smca
```

### Partitions ###
Partitions are a set of possible tracks in a tracking algorithms window of reference. A standard track is initialized in the form:

```
#!python

mypart = mca.partition([numTracks, framesDropped, pixelsPerFrame, time, P_val, Q_val, R_val, gamma])
```
* int __numTracks__ is the number of tracks at the start of the program (should not be used unless implemented for other uses).
* int __framesDropped__ is the number of frames an object can drop and still continue as a track, raising this allows more connections to following observations.
* float __pixelsPerFrame__ is the number of pixels an object can move per frame.
* int __time__ is the number of frames in the partition as initialized. (should not be used unless implemented for other uses).
* float **P\_val** is the covariance of the kalman filter used.
* float **R\_val** is the observation noise used in the kalman filter.
* float **gamma** is the death rate of a track at any given frame.

### Adding Observations ###
Observations are added by frame and can either be added directly to a track or to false alarms, where it is used for predictions in the future.
Observations should be in the form of a __2x1 numpy matrix__ and inserted into a list of observations for false alarms and a list of ordered observations for tracks (ordered by track ID)

```
#!python

mypart.addFrame([falseAlarms, orderedObs])
```
* list __falseAlarms__ is the list of observations to be added to false alarms for the new frame.
* list __orderedObs__ is the list of ordered observations to be added to the tracks for the new frame.

### Creating Predictions ###

Once a partition with false alarms is created, you can predict the paths of the objects through the parameters provided using the predict function.

```
#!python

mypart = mca.predict(<part>, <iterations>, [p_z, p_d, lambda_b, lambda_f])
```
Note that the method returns a partition instance that contains the tracks and false alarms created through the iterations.

* partition __part__ is the partition object created previously.
* int __iterations__ is how many moves should be done to create the predicted partition
* float **p\_z** is the probability an object disappears from sight at any given time. Adjust this to modify how tracks allow for occlusion and endings. [0.0, 1.0]
* float **p\_d** is the probability an object is detected at any given time. Adjust this to modify how tracks can miss objects to track. [0.0, 1.0]
* float **lambda\_b** is the birth-rate of objects per frame per volume. Adjust this to modify the number of tracks in the partition.
* float **lambda\_f** is the false alarm rate per frame per volume. Adjust this to modify the number of false alarms in the partition.


### Importing Observations from Pandas Data ###

The convertCSV method can be used to convert MOT Challenge-style CSV file data (2016/2017 challenges) into partition objects.
Namely, the csv file should have columns in order:

__frame | ID | x | y | width | height | conf | 1 | 2 | 3__

where an ID of -1 indicates an unidentified observation.
Note that this method changes all observations into respective false alarms in the partition (ignoring the ID column) for later predictions.
The CSV data can be inserted, then used as follows:

```
#!python

mypart = mca.convertCSV(<csvFile>, [startFrame, endFrame, framesDropped, pixelsPerFrame, P_val, Q_val, R_val])
```

* string __csvFile__ is the directory path to the csv file in your system.
* int __startFrame__ is the first numbered frame in the CSV file for which the partition will begin.
* int __endFrame__ is is the last numbered frame in the CSV file for which the partition will end.
* int __framesDropped__ is the number of frames a track can drop to account for occlusion.
* float __pixelsPerFrame__ is the number of pixels per frame an object can move in the recorded footage.
* float **P\_val** is the covariance of the kalman filter used.
* float **Q\_val** is the observation noise covariance used in the kalman filter.
* float **R\_val** is the observation noise used in the kalman filter.


### Importing Tracks from ID'd Pandas Data ###

This method creates a partition object as above, however, with tracks created from the IDs of the observations. This is primarily used for creating ground truth instances to test predictions.
The CSV data can be inserted, then used as follows:

```
#!python

mypart = mca.convertGTCSV(<csvFile>, [startFrame, endFrame])
```

* string __csvFile__ is the directory path to the CSV file from which the partition is made.
* int __startFrame__ is the first numbered frame in the CSV file for which the partition will begin.
* int __endFrame__ is the last numbered frame in the CSV file for which the partition will end.

Note: there are no options for other values made when making a partition class due to the fact that they are unnecessary when complete track are already made and predictions will not be done.

### Creating a Shifting Window Partition (Beta) ###

A similar class to the MCMC partition class, shiftMCMC partition, can be used to create a shifting window over footage for live tracking. In order to use this class, through mcmct import the shiftMCMCalg and create a partition using this.

```
#!python

from mcmct import shiftMCMCalg as smca

windowed_partition = smca.partition([framesDropped, pixelsPerFrame, time, windowSize, safetyFrames, P_val, Q_val, R_val, gamma])
```

* int __framesDropped__ is the number of frames a track can drop to account for occlusion.
* float __pixelsPerFrame__ is is the number of pixels per frame an object can move in the recorded footage.
* int __time__ is the number of frames in the partition as initialized. (should not be used unless implemented for other uses).
* int __windowSize__ is the allocated number of frames used in the shifting window to create the partition.
* int __safetyFrames__ is the number of head frames that are unchanged by the moves in the MCMC sampling.
* float __P\_val__ is the covariance of the kalman filter used.
* float __Q\_val__ is the observation noise covariance used in the kalman filter.
* float __R\_val__ is the observation noise used in the kalman filter.
* float __gamma__ is the death rate of a track at any given frame.

### Code Example ###

Tracking movement of basic circular movements on screen.

```
#!python

from Module import MCMCalg as mca
import numpy as np
from math import sin, cos, radians

mypart = mca.partition(numTracks=0, framesDropped=1, pixelsPerFrame=.2)

for i in range(60):
	observations = [np.matrix([[cos(radians(360. * i / 60.))], [sin(radians(360. * i / 60.))]]), \
					np.matrix([[-.8*cos(radians(360. * i / 60.))], [-.8*sin(radians(360. * i / 60.))]]), \
					np.matrix([[.5*cos(radianse(360. * i / 60.)], [.5*sin(radians(360. * i / 60.))]]), \
					np.matrix([[-.3*cos(radianse(360. * i / 60.)], [-.3*sin(radians(360. * i / 60.))]])]
	mypart.addFrame(falseAlarms=observations)
	
mypart = mca.predict(mypart, 1000, p_z=0.001, p_d=0.999, lambda_b=0.8, lambda_f=0.0001)
```

### Raw points ###
![alt text](https://bytebucket.org/tlb-intern2017/mcmctracker/raw/95ebb86cb90db6374dfa77bf1e4a33cfae14526c/graph_images/raw_data.png?token=526b6b33385f14fed4f912da82b1c7c09fb898fa)

### Partition after 1000 iterations in mca.predict ###
![alt text](https://bytebucket.org/tlb-intern2017/mcmctracker/raw/95ebb86cb90db6374dfa77bf1e4a33cfae14526c/graph_images/track.png?token=032ef4aa385a21ead346308767249bfd6a67f3cf)

Graphs are made using plotly

## Contacts ##

* Contact us at ttr5n@virginia.edu
