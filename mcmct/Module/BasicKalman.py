#Basic Kalman
from math import *
import numpy as np
from numpy import matrix

def kalman_filter(mean, cov, measurements, stateTrans, motion, measureFun, noise=np.matrix([[1.]])):
	for n in range(len(measurements)):
		mean, cov = update(measurements[n], measureFun, mean, cov, noise)
		mean, cov = predict(mean, cov, stateTrans, motion)
		# print mean
		# print cov
	return mean, cov


def predict(mean, cov, stateTrans, motion=0, noiseCov=0, noiseFun=0):
	return stateTrans * mean + motion, stateTrans * cov * stateTrans.T + noiseFun * noiseCov * noiseFun.T

def update(measurement, measureFun, mean, cov, noiseCov=0):
	y = measurement - measureFun * mean
	S = measureFun * cov * measureFun.T + noiseCov
	K = cov * measureFun.T * S.getI()
	I = np.identity(K.shape[0])
	return mean + (K * y), (I - K * measureFun) * cov
