#visualize.py
import os
import sys
import pandas as pd
import glob
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import matplotlib.image as mpimg
from matplotlib import animation
from moviepy.editor import VideoFileClip

def getRGBfromI(intval):
	color = ["g", "b", "c", "m", "y", "k", "w"]
	return color[intval % 7]

def visualize(imgDirectory, csvFile, animFile, startFrame=1, endFrame=None):
	# sanity check
	if not os.path.exists(imgDirectory):
		raise Exception("%s does not exist." % imgDirectory)
	if not os.path.exists(csvFile):
		raise Exception("%s does not exist." % csvFile)
	if not csvFile.endswith('.txt') and not detfile.endwith('.csv'):
		raise Exception("%s incorrect type." % csvFile)
	if not animFile.endswith('.mp4') and not animFile.endswith('.avi'):
		raise Exception("%s incorrect type." % animFile)
	if not endFrame == None and (not isinstance(endFrame, int) or endFrame <= startFrame):
		raise Exception("%s improper input." % endFrame)

	folder = imgDirectory

	# Read in csv file
	data = pd.read_csv(csvFile, names=('frame', 'ID', 'x', 'y', 'width', 'height', 'conf', '1', '2', '3'))

	# import directory of images
	if not folder.endswith('/'):
		folder += '/'

	frameNum = startFrame
	imageName = folder + str(frameNum).zfill(6) + '.jpg'

	if not os.path.isdir(folder + 'detCollection/'):
		os.makedirs(folder + 'detCollection/')

	dpi = 80

	while(os.path.exists(imageName)):

		# read in image and set size
		img = mpimg.imread(imageName)
		height, width, nbands = img.shape
		figsize = width / float(dpi), height / float(dpi)
		minidata = data[data['frame'] == frameNum]

		fig, ax = plt.subplots(1, figsize=figsize, dpi=dpi)

		
		# take axis off
		plt.axis('off')

		# adjust boundaries
		plt.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=None, hspace=None)

		# compute boxes for recognition
		for i, row in minidata.iterrows():
			color = 'r'
			if row['ID'] != -1:
				color = getRGBfromI(int(row['ID']))
			ax.add_patch(patches.Rectangle((row['x'], row['y']), 
				row['width'], row['height'], linewidth=1, 
				edgecolor=color, facecolor='none'))


		# display the image
		imgplot = plt.imshow(img, animated=True)
		sys.stdout.write('.')

		# save image
		fig.savefig(folder + 'detCollection/' + 'det' + str(frameNum).zfill(6) + '.jpg', pad_inches=0, bbox_inches='tight')

		# next image
		frameNum += 1
		imageName = folder + str(frameNum).zfill(6) + '.jpg'

		if endFrame != None and frameNum == endFrame + 1:
			break

	imagefiles = glob.glob(folder + 'detCollection/*.jpg')
	imgs = []
	fig = plt.figure(figsize=figsize, dpi=80)
	fig.subplots_adjust(left=0, bottom=0, right=1, top=1, wspace=None, hspace=None)
	plt.axis('off')

	for f in imagefiles:
		img = mpimg.imread(f)
		im = plt.imshow(img, animated=True)
		imgs.append([im])

	fps = 30
	anim = animation.ArtistAnimation(fig, imgs, blit=True, repeat=False, interval=1000/fps)
	anim.save(animFile)

	print "Process Completed"




if __name__ == '__main__':

	if len(sys.argv) < 4:
		print "Incorrect Number of arguments"
		exit()

	startFrame = 1
	endFrame = None
	outputImage = False

	if len(sys.argv) > 4:
		for i in sys.argv:
			if i.startswith("startFrame="):
				startFrame = int(i.replace("startFrame=", ""))
			if i.startswith("endFrame="):
				endFrame = int(i.replace("endFrame=", ""))

	visualize(sys.argv[1], sys.argv[2], sys.argv[3], startFrame, endFrame)

