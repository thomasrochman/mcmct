# MCMCalg with shifting window

import os
import sys
import numpy as np
import pdb
from scipy.stats import multivariate_normal
from Module import BasicKalman as bk
from random import randint, choice, random
import math
import copy
import traceback
from tqdm import trange, tqdm
import pandas as pd
from copy import deepcopy
import platform
if platform.python_version()[0] == '3':
	import queue
else:
	import Queue as queue

def compare_distance(node1, node2):
	'''
	Method used to find whether or not a path can be connected from node1 to node2.
	Finds the euclidean distance.
	'''
	return math.sqrt((node1.item(0) - node2.item(0)) ** 2 + (node1.item(1) - node2.item(1)) ** 2)

class partition(object):

	def __init__(self, framesDropped=0, pixelsPerFrame=1, time=0, windowSize=20, safetyFrames=5, P_val=1000., Q_val=0.01, R_val=1., gamma = 0.001):
		'''
		Inititalization of the partition class.
		Parameters:

		numTracks: number of tracks at initialization (set to 0 if creating a proper partition, only utilized when creating ground truth, etc.)
		framesDropped: Number of frames the algorithm can skip to connect observations (to include occlusion)
		pixelsPerFrame: number of pixels an object can move per frame 
		time: Initial amount of frames passed (keep at 0)
		windowSize: Frames in the window
		P_val: 
		Q_val:
		R_val:
		gamma: the percentage at which a person disappears from the image at any given frame.
		'''
		self.falseAlarms = []
		self.tracks = [[None] * time for i in range(numTracks)]
		self.mxFrame = framesDropped
		self.pxlFrame = pixelsPerFrame
		self.duration = time
		self.window = windowSize
		self.safety = safetyFrames
		self.filters = [None for i in range(numTracks)]
		self.P_val = P_val
		self.Q_val = Q_val
		self.R_val = R_val
		self.gamma = gamma
		self.liveTracks = [False for i in range(numTracks)]
		self.currentStartFrame = time
		# call an item by partition.tracks[object][time]

	def validateNext(self, obs1, time1, obs2, time2):
		'''
		Method used to check whether or not obs1 can be connected to obs2.
		obs1: first chronological observation
		time1: frame of first chronological observation
		obs2: second chronological observation
		time2: second chronological time
		'''
		if (compare_distance(obs1, obs2) <= self.pxlFrame * abs(time1 - time2)) and (abs(time1 - time2) <= self.mxFrame) and (time1 != time2):
			return True
		else:
			return False

	def getObjDurations(self):
		'''
		Method to get actual durations of tracks and returning a list of all inital and final observation frames in order.
		'''
		objDurations = []
		for i in [j for j, _x in enumerate(self.liveTracks) if _x]:
			track = self.tracks[i]
			validIds = [i for i,_x in enumerate(track) if _x is not None]
			(iFrom, iTo) = (min(validIds), max(validIds))
			objDurations.append((iFrom, iTo))
		return objDurations

	def kFilter(self, trackNum):
		'''
		Finds the Kalman filter of the given track in the partition.
		trackNum: index number of the track in the partition tracks parameter
		'''
		P_init = np.diag([self.P_val, self.P_val, self.P_val, self.P_val])
		Q = np.diag([self.Q_val, self.Q_val])
		C = np.matrix([[1., 0., 0., 0.],
						[0., 1., 0., 0.]])
		# R = np.diag([25., 25.])
		R = np.diag([self.R_val, self.R_val])
		timesNoNone = [l for l, _x in enumerate(self.tracks[trackNum]) if _x is not None]
		x_bar = np.matrix([[self.tracks[trackNum][min(timesNoNone)].item(0), self.tracks[trackNum][min(timesNoNone)].item(1), 0., 0.]]).T
		P_bar = P_init

		answer = 1
		answer2 = 0
		for i, t_i in enumerate(timesNoNone[:-1]):
			j = i + 1
			t_j = timesNoNone[j]
			delta = (float)(t_j - t_i)
			A = np.matrix([[1., 0., delta, 0.], 
							[0., 1., 0., delta], 
							[0., 0., 1., 0.], 
							[0., 0., 0., 1.]])
			G = np.matrix([[(delta ** 2) / 2, 0.], 
							[0., (delta ** 2) / 2], 
							[delta, 0.], 
							[0., delta]])
			x_hat, P_hat = bk.update(measurement=self.tracks[trackNum][t_i], measureFun=C, mean=x_bar, cov=P_bar, noiseCov=R)
			x_bar, P_bar = bk.predict(mean=x_hat, cov=P_hat, stateTrans=A, noiseCov=Q, noiseFun=G)

			B = C * P_bar * C.T + R
			y_bar = (C*x_bar).T.tolist()[0]
			y = self.tracks[trackNum][t_i].T.tolist()[0]
			var = multivariate_normal(mean=y_bar, cov=B)
			answer *= var.pdf(y)
			answer2 += var.logpdf(y)
		# print("kalman:", math.log(answer))
		# print("kalmanLog:", answer2)
		return answer2

	def addFrame(self, orderedObs=[], falseAlarms=[]):
		'''
		Method to add a frame of observations into the partition.
		obs: The false alarms/observations that cannot yet be attributed to a track of the frame.
		'''
		for i in range(len(orderedObs)):
			self.tracks[i].append(orderedObs[i])
			if self.liveTracks[i]:
				self.filters[i] = self.kFilter(i)
		for j in range(len(orderedObs), len(self.tracks)):
			self.tracks[j].append(None)

		self.falseAlarms.append(obs)
		self.duration += 1
		if self.duration > self.window:
			self.deleteFrame()

	def deleteFrame(self):
		'''
		deletes first frame to make way for more space.
		'''
		for i in range(len(self.tracks)):
			self.tracks[i].pop(0)
			self.filters[i] = self.kFilter(i)
		self.falseAlarms.pop(0)
		self.duration -= 1
		for i in [j for j, _x in enumerate(liveTracks) if _x]:
			obsloc = [k for k, _val in enumerate(self.tracks[i]) if _val is not None]
			if len(obsloc) <= 1 or max(obsloc):
				self.liveTracks[i] = False
			elif max(obsloc) + self.mxFrame <= self.safety:
				self.liveTracks[i] = False
		self.currentStartFrame += 1

	def clone(self):
		'''
		Creates and returns a deepcopy of the partition.
		'''
		return copy.deepcopy(self)

	'''
	All moves create a new instance with the move
	'''

	def birth(self):
		'''
		Move to birth a track from false alarms (minimum two observations)
		'''
		newTrack = [None for i in range(self.duration)]
		pairs = []
		for i in range(self.safety + 1, len(self.falseAlarms)):
			for node1 in range(len(self.falseAlarms[i])):
				for j in range(i + 1, len(self.falseAlarms)):
					for node2 in range(len(self.falseAlarms[j])):
						if self.validateNext(self.falseAlarms[i][node1], i, self.falseAlarms[j][node2], j):
							pairs.append((i, node1, j, node2))

		if len(pairs) == 0:
			raise Exception("No candidate pairs for birth.")

		insert = choice(pairs)
		timeChoice = insert[2]
		nextObs = self.falseAlarms[insert[2]][insert[3]]
		newTrack[insert[0]] = self.falseAlarms[insert[0]].pop(insert[1])
		newTrack[insert[2]] = self.falseAlarms[insert[2]].pop(insert[3])
		decider = random()

		while decider > self.gamma:
			timeChoices = [i for i, _x in enumerate(self.falseAlarms) if len(_x) != 0 and i > timeChoice]
			obsChoices = []
			if len(timeChoices) == 0:
				break
			for i in timeChoices:
				newList = [j for j, _y in enumerate(self.falseAlarms[i]) if self.validateNext(nextObs, timeChoice, _y, i)]
				if len(newList) != 0:
					obsChoices.append((i, newList))
			if len(obsChoices) == 0:
				break

			listChoice = randint(0, len(obsChoices) - 1)
			timeChoice = obsChoices[listChoice][0]
			nextObsNum = choice(obsChoices[listChoice][1])
			nextObs = self.falseAlarms[timeChoice].pop(nextObsNum)
			newTrack[timeChoice] = nextObs
			decider = random()

		self.tracks.append(newTrack)
		self.filters.append(self.kFilter(-1))
		self.liveTracks.append(True)

	def death(self):
		'''
		Move to delete a track from tracks and return all obs to respective fals alarm times
		'''
		possibleTracks = [j for j, tau in enumerate(self.tracks) if len([val for i, val in enumerate(tau) if val is not None and i <= self.safety]) == 0]
		if len(possibleTracks) == 0:
			raise Exception("No candidate tracks.")
		trackID = choice(possibleTracks)
		for (i, obs) in [(j, ob) for j, ob in enumerate(self.tracks[trackID]) if ob is not None]:
			self.falseAlarms[i].append(obs)
		self.tracks.pop(trackID)
		self.filters.pop(trackID)
		self.liveTracks.pop(trackID)

	def split(self):
		'''
		Move to split a track into two seperate tracks at a given point
		'''
		candidates = [trackID for trackID, track in enumerate(self.tracks) if len([x for x in track if x is not None]) > 3 and len([y for i, y in track if y is not None and i > self.safety]) > 1 and self.liveTracks[trackID] is True]

		if len(candidates) == 0:
			raise Exception("No candidate tracks.")
		trackID = choice(candidates)
		candLoc = [i for i, obs in enumerate(self.tracks[trackID]) if obs is not None and i > self.safety]
		location = candLoc[randint(1, len(candLoc) - 3)]
		newTrack = [None for j in range(self.duration)]
		startloc = location + 1 if location > self.safety else self.safety + 1
		for t in range(startloc, self.duration):
			newTrack[t] = self.tracks[trackID][t]
			self.tracks[trackID][t] = None
		self.tracks.append(newTrack)
		self.filters[trackID] = self.kFilter(trackID)
		self.filters.append(self.kFilter(-1))
		self.liveTracks.append(True)

	def merge(self):
		'''
		Move to merge two compatible tracks together
		'''
		pairs = []
		live = [k for k, _x in enumerate(self.liveTracks) if _x]
		for i in live:
			for j in live:
				if i != j:
					maxi = max([k for k, _x in enumerate(self.tracks[i]) if _x is not None])
					minj = min([k for k, _x in enumerate(self.tracks[j]) if _x is not None])

					if minj > self.safety and maxi < minj and self.validateNext(self.tracks[i][maxi], maxi, self.tracks[j][minj], minj):
						pairs.append((i, j))

		if len(pairs) == 0:
			raise Exception("No candidate track pairs.")

		pair = choice(pairs)
		for i in range(min([j for j, _x in enumerate(self.tracks[pair[1]]) if _x is not None]), self.duration):
			self.tracks[pair[0]][i] = self.tracks[pair[1]][i]
		self.filters[pair[0]] = self.kFilter(pair[0])
		self.tracks.pop(pair[1])
		self.filters.pop(pair[1])
		self.liveTracks.pop(pair[1])

	def extension(self):
		'''
		Move to extend a given track with applicable false alamrs.
		'''

		possibleTracks = [trackID for trackID, track in enumerate(self.tracks) if self.liveTracks[trackID] and max([i for i, _x in enumerate(track) if _x is not None]) != self.duration - 1]

		if len(possibleTracks) == 0:
			raise Exception("No candidate tracks.")
		trackID = choice(possibleTracks)
		lastTime = max([i for i, _x in enumerate(self.tracks[trackID]) if _x is not None])
		candidates = []
		for i in range(lastTime + 1, self.duration):
			possibles = [j for j, _x in enumerate(self.falseAlarms[i]) if self.validateNext(self.tracks[trackID][lastTime], lastTime, _x, i)]
			if len(possibles) != 0:
				candidates.append((i, possibles))
		decider = random()
		while decider > self.gamma and len(candidates) != 0:
			chosen = choice(candidates)
			chosenObjID = choice(chosen[1])
			lastTime = chosen[0]
			self.tracks[trackID][lastTime] = self.falseAlarms[lastTime][chosenObjID]
			self.falseAlarms[lastTime].pop(chosenObjID)

			candidates = []
			for i in range(lastTime + 1, self.duration):
				possibles = [j for j, _x in enumerate(self.falseAlarms[i]) if self.validateNext(self.tracks[trackID][lastTime], lastTime, _x, i)]
				if len(possibles) != 0:
					candidates.append((i, possibles))
			decider = random()

		self.filters[trackID] = self.kFilter(trackID)
		
	def reduction(self):
		'''
		Move to reduce a track to a given observation
		'''

		candidates = [trackID for trackID, track in enumerate(self.tracks) if (len([i for i, val in enumerate(track) if val is not None and i > self.safety]) > 0 and \
						len([i for i, val in enumerate(track) if val is not None and i <= self.safety]) >= 0) or \
						len([i for i, val in enumerate(track) if i > self.safety]) >= 3]
		if len(candidates) == 0:
			raise Exception("No candidate tracks")
		trackID = choice(candidates)
		trackIndNoNone = [i for i, obs in enumerate(self.tracks[trackID]) if obs is not None]
		location = choice([j for i, j in enumerate(candLoc) if i >= 2 and j > self.safety])
		for j in range(location, self.duration):
			if self.tracks[trackID][j] is not None:
				self.falseAlarms[j].append(self.tracks[trackID][j])
				self.tracks[trackID][j] = None
		self.filters[trackID] = self.kFilter(trackID)

	def update(self):
		'''
		Move to reduce a track to a given observation, then extend the track.
		'''

		trackID = choice([i for i, _x in enumerate(self.liveTracks) if _x])
		startPos = choice(range(self.safety + 1, min(max([i for i, _x in enumerate(self.tracks[trackID])]) + self.mxFrame + 1, self.duration)))

		for i in range(startPos, self.duration):
			if self.tracks[trackID][i] is not None:
				self.falseAlarms[i].append(self.tracks[trackID][i])
			self.tracks[trackID][i] = None

		positions = [i for i, _x in enumerate(self.tracks[trackID]) if _x is not None]
		if len(positions) <= 1:

			for i in positions:
				self.falseAlarms[i].append(self.tracks[trackID][i])
				self.tracks[trackID][i] = None
			self.birth()
			newTrack = self.tracks.pop()
			self.tracks[trackID] = newTrack
			self.filters[trackID] = self.filters.pop()
			self.liveTracks.pop()
			return
		else:
			startPos = max(positions)

		candidates = []
		end = min((self.duration, startPos + self.mxFrame + 1))
		for i in range(startPos, end):
			if len(self.falseAlarms[i]) != 0:
				possibles = [j for j, _x in enumerate(self.falseAlarms[i]) if self.validateNext(self.tracks[trackID][startPos], startPos, _x, i)]
				if len(possibles) != 0:
					candidates.append((i, possibles))
		decider = random()
		while decider > self.gamma and len(candidates) != 0:
			chosenPair = choice(candidates)
			chosenObjID = choice(chosenPair[1])
			startPos = chosenPair[0]
			self.tracks[trackID][startPos] = self.falseAlarms[startPos][chosenObjID]
			self.falseAlarms[startPos].pop(chosenObjID)

			candidates = []
			for i in range(startPos, min(self.duration, startPos + self.mxFrame + 1)):
				possibles = [j for j, _x in enumerate(self.falseAlarms[i]) if self.validateNext(self.tracks[trackID][startPos], startPos, _x, i)]
				if len(possibles) != 0:
					candidates.append((i, possibles))
			decider = random()
		self.filters[trackID] = self.kFilter(trackID)

	def switch(self):
		'''
		Move that takes two tracks and switches observations in the track after given frame t.
		'''

		possible = []
		# list of possible switches: (position_of_split, track1#, track2#)])
		for tau1 in [i for i, _x in enumerate(self.liveTracks) if _x]:
			for position in range(self.safety + 1, self.duration - 1):
				startTrack1 = [i for i, _x in enumerate(self.tracks[tau1][:position]) if _x is not None]
				maxStart1 = None if len(startTrack1) == 0 else max(startTrack1)
				endTrack1 = [i for i, _x in enumerate(self.tracks[tau1][position:]) if _x is not None]
				minEnd1 = None if len(endTrack1) == 0 else min(endTrack1)
				for tau2 in range([j for j, _y in enumerate(self.liveTracks) if _y]):
					if tau1 == tau2:
						continue
					startTrack2 = [i for i, _x in enumerate(self.tracks[tau2][:position]) if _x is not None]
					maxStart2 = None if len(startTrack2) == 0 else max(startTrack2)
					endTrack2 = [i for i, _x in enumerate(self.tracks[tau2][position:]) if _x is not None]
					minEnd2 = None if len(endTrack2) == 0 else min(endTrack2)
					comb1 = False
					comb2 = False

					if maxStart1 is None or minEnd2 is None or self.validateNext(self.tracks[tau1][maxStart1], maxStart1, self.tracks[tau2][minEnd2], minEnd2):
						comb1 = True
					if minEnd1 is None or maxStart2 is None or self.validateNext(self.tracks[tau1][minEnd1], minEnd1, self.tracks[tau2][maxStart2], maxStart2):
						comb2 = True
					if (minEnd1 is None and maxStart2 is None) or (minEnd1 is None and len(startTrack2) == 1) or (maxStart2 is None and len(endTrack1) == 1):
						comb2 = False
					if (maxStart1 is None and minEnd2 is None) or (maxStart1 is None and len(endTrack2) == 1) or (minEnd2 is None and len(startTrack1) == 1):
						comb1 = False
					if comb1 and comb2:
						possible.append((position, tau1, tau2))

		if len(possible) == 0:
			raise Exception("No candidate tracks.")
		
		to_switch = choice(possible)

		for i in range(to_switch[0], self.duration):
			temp =self.tracks[to_switch[2]][i]
			self.tracks[to_switch[2]][i] = self.tracks[to_switch[1]][i]
			self.tracks[to_switch[1]][i] = temp
		self.filters[to_switch[1]] = self.kFilter(to_switch[1])
		self.filters[to_switch[2]] = self.kFilter(to_switch[2])


def posterior_part1(om, T, p_z, p_d, lambda_b, lambda_f):
	'''
	First portion of the posterior calculation. Returned answer is the logarithmic value of the portion.

	Parameters:
	om: list of lists of two dimensional vectors/matrices of observations, [track][time]. Partition datatype
	T: int, max time
	p_z: float, probability a target disappears at a given moment (< 1)
	p_d: float, probablity an object is detected at a given moment (< 1)
	z: list, number of targets terminated for each time t
	d: list, number of detections for each time t
	u: list, number of undetected targets at each time t (= e - z + a - d)
	a: list, number of new targets for each time t
	f: list, number of false alarms for each time t (= n - d)
	lambda_b: float, birthrate of new objects per time-volume
	lambda_f: float, false alarms per time-volume
	'''
	
	Z = 1 # Subject to Change

	d = []
	z = []
	a = []
	e = []
	objDurations = om.getObjDurations()
	for time in range(om.duration):
		d.append(len([1 for i, track in enumerate(om.tracks) if track[time] is not None and self.liveTracks[i]]))
		if time == 0:
			z.append(0)
			a.append(len([1 for i, track in enumerate(om.tracks) if track[time] is not None and self.liveTracks[i]]))
			e.append(0)
		else:
			a.append( len([1 for i, track in enumerate(om.tracks) if track[time-1] is None and track[time] is not None and self.liveTracks[i]]) )
			z.append( len([1 for i, dur in enumerate(objDurations) if time-1 == dur[1] and self.liveTracks[i]]) )
			e.append( len([1 for i, dur in enumerate(objDurations) if dur[0] <= time-1 and time-1 <= dur[1] and self.liveTracks[i]]) )

	# answer = 1/Z
	answer2 = 0

	for t in range(1, T + 1):
		answer2 += math.log((p_z ** z[t]) * ((1 - p_z) ** (e[t] - z[t])) * (p_d ** d[t]) * ((1 - p_d) ** (e[t] - z[t] + a[t] - d[t])) \
				* (lambda_b ** a[t]) * (lambda_f ** len(om.falseAlarms[t])))

	return answer2

def predict(partition, sampleNum, p_z=0.001, p_d=0.999, lambda_b=0.8, lambda_f=0.0001):
	'''
	Function to create a partition after sampleNum amount of random moves.

	Parameters:
	partition: partitions object to be changed through moves.
	sampleNum: Number of moves to be done on the partition.
	p_z: probabil.ity a target disappears at any given time.
	p_d: probability and given object is detected at any given time.
	lambda_b: birthrate of an object per unit time-volume.
	lambda_f: number of false alarms per unit time-volume.
	'''

	post = sum([val for i, val in enumerate(partition.filters) if partition.liveTracks[i]]) + posterior_part1(partition, partition.duration - 1, p_z, p_d, lambda_b, lambda_f)
	best = (post, partition)
	current = partition
	for n in trange(1, sampleNum + 1):
		proposal = current.clone()
		proposedMove = 0
		if len([track for i, track in enumerate(proposal.tracks) if proposal.liveTracks[i]]) == 0:
			proposedMove = 1 
		elif len([track for i, track in enumerate(proposal.tracks) if proposal.liveTracks[i]]) == 1:
			proposedMove = choice([1, 2, 3, 5, 6, 7])
		else:
			proposedMove = choice(range(1, 9))
		moves = {1 : "Birth", 2 : "Death", 3 : "Split", 4 : "Merge", 5 : "Extension", 6 : "Reduction", 7 : "Update", 8 : "Switch"}

		try:
			if proposedMove == 1:
				proposal.birth()
			elif proposedMove == 2:
				proposal.death()
			elif proposedMove == 3:
				proposal.split()
			elif proposedMove == 4:
				proposal.merge()
			elif proposedMove == 5:
				proposal.extension()
			elif proposedMove == 6:
				proposal.reduction()
			elif proposedMove == 7:
				proposal.update()
			else:
				proposal.switch()
		except:
			# print(traceback.format_exc())
			pass

		if len(proposal.tracks) != len(proposal.filters):
			print("Incorrect Filter Number. Last move:", moves[proposedMove])

		propo2 = sum([filt for i, filt in enumerate(proposal.filters) if proposal.liveTracks[i]])
		cur2 = sum([filt for i, filt in enumerate(current.filters) if current.liveTracks[i]])
		propo1 = posterior_part1(proposal, proposal.duration - 1, p_z, p_d, lambda_b, lambda_f)
		cur1 = posterior_part1(current, current.duration - 1, p_z, p_d, lambda_b, lambda_f)

		try:
			A = min((1, math.exp(propo1 + propo2 - cur2 - cur1)))
		except OverflowError:
			A = float('inf')

		if propo1 + propo2 > best[0]:
			best = (propo1 + propo2, proposal)

		if random() < A or (len(current.tracks) == 0 and proposedMove == 1):
			current = proposal

	return best[1]

def calcAccuracy(partition, ground_truth):
	'''
	Function to compare a given partition object to the ground truth.

	Parameters:
	partition: the current partition to be compared to the ground truth
	ground_truth: the ground truth of the tracks. In the form of a partition object from MCMCalg.

	returns the percentage for which the partition is similar to the ground_truth in the form of a decimal.
	'''
	correct = 0
	allObs = 0
	partitionLive = [val for i, val in enumerate(partition.tracks) if partition.liveTracks[i]]
	GTLive = [track[partition.currentStartFrame : partition.duration] for track in ground_truth.tracks if max([i for i, val in enumerate(track[partition.currentStartFrame : partition.duration]) if val is not None]) + partition.mxFrame > partition.safety]

	availableTP = queue.Queue(maxsize=len(partitionLive))
	exhausted = queue.Queue(maxsize=len(partitionLive))
	for i in range(len(partitionLive)):
		availableTP.put_nowait(i)

	gtPairedTo = [None for i in range(len(GTLive))] # which track from ground_truth is paired to which track in paritition (numbers in list are indices from partition)
				#put in as a pair (partition_track_index, score)
	for tau in GTLive:
		allObs += len([j for j, _x in enumerate(tau) if _x is not None])

	while not availableTP.empty():
		fromPart = availableTP.get_nowait()
		to_keep = True
		for i in range(len(GTLive)):
			score = len([j for j, _x in enumerate(GTLive[i]) if \
						(GTLive[i][j] is not None and partitionLive[fromPart][j] is not None and all(GTLive[i][j] == partitionLive[fromPart][j]))])
			if (gtPairedTo[i] is None or gtPairedTo[i][1] < score) and score != 0:
				if gtPairedTo[i] is not None: availableTP.put_nowait(gtPairedTo[i][0])
				gtPairedTo[i] = (fromPart, score)
				while not exhausted.empty():
					availableTP.put_nowait(exhausted.get_nowait())
				to_keep = False
				break
		if to_keep:
			if len([i for i, _x in enumerate(gtPairedTo) if _x is None]) == 0:
				exhausted.put_nowait(fromPart)
			else:
				gtPairedTo[[i for i, _x in enumerate(gtPairedTo) if _x is None][0]] = (fromPart, 0)
				
	for pair in [x for x in gtPairedTo if x is not None]:
		correct += pair[1]
	return float(correct)/float(allObs)

