# Thomas Rochman
# Basic Tracker

import os
import sys
import pandas as pd

def plotter(csvFile, outputFile):
	
	if not os.path.exists(csvFile):
		raise Exception("%s does not exist." % csvFile)
	if not (csvFile.endswith('.txt') or csvFile.endswith('.csv')):
		raise Exception("%s id not a proper file type.\nUse .txt or .csv file." % csvFile)
	if not (outputFile.endswith('.txt') or outputFile.endswith('.csv')):
		raise Exception("%s id not a proper file type.\nUse .txt or .csv file." % outputFile)

	data = pd.read_csv(csvFile, names=('frame', 'ID', 'x', 'y', 'width', 'height', 'conf', '1', '2', '3'))
	frameNum = 1
	minidataPrev = data[data['frame'] == frameNum]
	minidataNext = data[data['frame'] == frameNum + 1]
	objCount = 0

	while frameNum < data['frame'].max():
		for i, rowPrev in minidataPrev.iterrows():
			for j, rowNext in minidataNext.iterrows():
				if abs(rowNext['x'] - rowPrev['x'] < 1) and \
					abs(rowNext['y'] - rowPrev['y']) < 1 and \
					abs(rowNext['width'] - rowPrev['width']) < 1 and \
					abs(rowNext['height'] - rowPrev['height']) < 1 and \
					rowNext['ID'] == -1:
					if rowPrev['ID'] == -1:
						data['ID'][i] = objCount
						objCount += 1
					data['ID'][j] = data['ID'][i]
					break
		sys.stdout.write('.')
		frameNum += 1
		minidataPrev = data[data['frame'] == frameNum]
		minidataNext = data[data['frame'] == frameNum + 1]
	print '\nDataset rewriting...'
	data.to_csv(outputFile, index=False, header=False)


if __name__ == '__main__':
	
	if len(sys.argv) != 3:
		raise Exception("Improper number of arguments.\n2 required: %d included." % (len(sys.argv) - 1))

	plotter(sys.argv[1], sys.argv[2])