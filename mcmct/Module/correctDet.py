import sys
import os
from tqdm import tqdm
import pandas as pd
import platform
if platform.python_version()[0] == '3':
	import queue
else:
	import Queue as queue
from shapely.geometry import box
import copy
import MCMCalg as mca

def find_overlap(xy1, width1, height1, xy2, width2, height2):
	rect1 = box(xy1[0], xy1[1], xy1[0] + width1, xy1[1] + height1)
	rect2 = box(xy2[0], xy2[1], xy2[0] + width2, xy2[1] + height2)
	intersect = (rect1.intersection(rect2)).area
	union = (rect1.union(rect2)).area
	return intersect / union

def correct(detFile, gtFile, newFile="newFile.txt"):
	if os.getcwd() not in detFile:
		detFile = os.path.join(os.getcwd(), detFile)
	if os.getcwd() not in gtFile:
		gtFile = os.path.join(os.getcwd(), gtFile)
		if os.getcwd() not in newFile:
		newFile = os.path.join(os.getcwd(), newFile)
	if not os.path.exists(detFile):
		raise Exception("Detection file does not exist.")
	if not os.path.exists(gtFile):
		raise Exception("Ground truth file does not exist.")
	if not detFile.endswith(".txt") and not detFile.endswith(".csv"):
		raise Exception("Detection file must be of type .txt or .csv")
	if not gtFile.endswith(".txt") and not gtFile.endswith(".csv"):
		raise Exception("Ground truth file must be of type .txt or .csv")
	if not newFile.endswith(".txt") and not newFile.endswith(".csv"):
		raise Exception("New file must be of type .txt or .csv")

	det = pd.read_csv(detFile, names=('frame', 'ID', 'x', 'y', 'width', 'height', 'conf', '1', '2', '3'))
	gt = pd.read_csv(gtFile, names=('frame', 'ID', 'x', 'y', 'width', 'height', 'conf', '1', '2', '3'))
	result = copy.deepcopy(det)
	# ----------------------------------------------------------------------
	# result['percentage2nd'] = pd.Series(0.0, index=result.index)
	# result['percentage1st'] = pd.Series(0.0, index=result.index)
	# ----------------------------------------------------------------------

	for i, row in tqdm(det.iterrows(), total=det.shape[0]):
		temp = 0
		for j, row2 in gt[gt['frame'] == row['frame']].iterrows():
			percentage = find_overlap((row['x'], row['y']), row['width'], row['height'], (row2['x'], row2['y']), row2['width'], row2['height'])
	# ----------------------------------------------------------------------
			# if percentage > result.iloc[i, 11]:
			# 	result.set_value(i, 10, result.iloc[i, 11], takeable=True)
			# 	result.set_value(i, 11, percentage, takeable=True)
	# ----------------------------------------------------------------------
			if percentage > .6 and percentage > temp:
				temp = percentage
				result.set_value(i, 1, gt.iloc[j, 1], takeable=True)
				# print(result.iloc[i, 1], "should be:", gt.iloc[j, 1])


	result.to_csv(newFile, index=False, header=False)
	print("Process Complete")
	return result



if __name__ == '__main__':
	if len(sys.argv) != 3 and len(sys.argv) != 4:
		raise Exception("Incorrect number of arguments. (3 or 4 required, %d provided)" % len(sys.argv) - 1)
	if len(sys.argv) == 3:
		correct(sys.argv[1], sys.argv[2])
	else:
		correct(sys.argv[1], sys.argv[2], sys.argv[3])